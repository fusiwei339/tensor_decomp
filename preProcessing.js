const db = require("./db");
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017";
const dbName = "mimic";
var moment = require("moment");

// drop original collections
var collectionName = "tweet";

const client = new MongoClient(url);
client.connect(function(err) {
  const db = client.db(dbName);
  const collection = db.collection(collectionName);
  collection.find().forEach(d => {
    d.timestamp = moment(d.time).unix();
    collection.save(d);
  });
  //   collection.insertMany(ret);

  client.close();
});

// db.tweet.find().forEach(d => {
//   d.timestamp = moment(d.time).unix();
//   d.hourStr = d.time.split(":")[0];
//   db.tweet.save(d);
// });
