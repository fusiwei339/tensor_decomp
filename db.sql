ssh -p7140 root@202.120.188.26


psql --username postgres
TJ.idvxlab
CREATE DATABASE "event_pred_db3";
\c event_pred_db
DROP TABLE patient;
CREATE TABLE patient(pid INT NOT NULL PRIMARY KEY, gender VARCHAR(10), birthday VARCHAR(30),code_string VARCHAR(290000));

CREATE TABLE IF NOT EXISTS patient(pid INT NOT NULL PRIMARY KEY, gender VARCHAR(10), birthday VARCHAR(30),code_string text, death_time VARCHAR(20));
\COPY patient(pid,gender,birthday,code_string,death_time) FROM '/home/zcjin/patient_sum.csv' DELIMITER ',' CSV

\COPY patient(pid,gender,birthday,code_string) FROM '/home/zcjin/patient_sum.csv' DELIMITER ',' CSV
select count(pid) from patient;
DROP TABLE event;
CREATE TABLE IF NOT EXISTS event(event_code int NOT NULL PRIMARY KEY, icd_code VARCHAR(32) NOT NULL,event_type VARCHAR(64) NOT NULL);
\COPY event(event_code,icd_code,event_type) FROM '/home/zcjin/event_sum.csv' DELIMITER ',' CSV
select count(event_code) from event;
DROP TABLE connect;
CREATE TABLE IF NOT EXISTS connect(pid int NOT NULL REFERENCES patient(pid),event_time VARCHAR(256) NOT NULL, event_code int NOT NULL REFERENCES event(event_code),icd_code VARCHAR(32) NOT NULL);
\COPY connect(pid,event_time,event_code,icd_code) FROM '/home/zcjin/connect_sum.csv' DELIMITER ',' CSV
select count(id) from connect;

create index patientindex on patient(pid, event_length);
create INDEX eventindex on event(event_code);
CREATE INDEX connectindex on connect(pid, event_time, event_code);


DROP TABLE patient;
CREATE TABLE IF NOT EXISTS patient(pid INT NOT NULL PRIMARY KEY, gender VARCHAR(10),code_string VARCHAR(500000));
\COPY patient(pid,gender,code_string) FROM '/home/zcjin/mimic_database/patient_sum.csv' DELIMITER ',' CSV
DROP TABLE event_tmp;
CREATE TABLE IF NOT EXISTS event_tmp(pid INT,event_time VARCHAR(256) NOT NULL, event_code  VARCHAR(64),event_type VARCHAR(64) NOT NULL);
\COPY event_tmp(pid,event_time,event_code,event_type) FROM '/home/zcjin/mimic_database/event_sum.csv' DELIMITER ',' CSV
DROP TABLE event;
CREATE TABLE IF NOT EXISTS event(event_code VARCHAR(64) NOT NULL PRIMARY KEY,event_type VARCHAR(64) NOT NULL);


scp -P 7140 zcjin@202.120.188.26:/home/zcjin/database/retain-api/model/mimic.6.npz /Users/tjvislab/Desktop/proj/2019-chi-eventthread-v3.0/


pg_dump -U postgres myDBname > filename.bak

# deploy
scp -P 7140 /Users/tjvislab/Desktop/proj/2019-chi-carepre/source/code.zip jsyang@202.120.189.180:projects/event_zoompred/

ssh -p7140 jsyang@202.120.189.180
ssh -p7140 jsyang@202.120.189.180
cd projects/event_zoompred/
unzip code.zip 
A
cd src/js/
uglifyjs patientinfo.js  --compress --mangle --output=patientinfo.js
uglifyjs compvis.js --compress --mangle --output=compvis.js
uglifyjs correlation.js --compress --mangle --output=correlation.js
uglifyjs  descrip.js --compress --mangle --output=descrip.js
uglifyjs histogram.js --compress --mangle --output=histogram.js
uglifyjs seqquery.js --compress --mangle --output=seqquery.js
uglifyjs seqvis.js --compress --mangle --output=seqvis.js
uglifyjs simvis.js  --compress --mangle --output=simvis.js
cd ../..
npm run build
pm2 delete event-thread
npm run start:pm2


scp -P 7140 zcjin@202.120.188.26:/home/zcjin/cut_spark/data/cut_index.json /Users/tjvislab/Desktop/proj/2019-chi-eventthread-v3.0/
scp -P 7140 Desktop/proj/2019-chi-eventthread-v3.0/source/index.html jsyang@202.120.189.180:projects/event_pred/


