from sklearn.feature_extraction.text import CountVectorizer
import numpy as np
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sktensor import sptensor
from ncp_hongjin import *
from pymongo import MongoClient
import preprocessor as p
from datetime import datetime

p.set_options(p.OPT.URL, p.OPT.EMOJI, p.OPT.RESERVED, p.OPT.MENTION)

clientLocal = MongoClient('localhost', 27017)
db = clientLocal.twitter

coll = 'frb'
fields = ['userId', 'hourStr', 'text', 'countryId']
fieldTypes = ['discrete', 'discrete', 'text', 'discrete']
rank = 20

# data preprocessing
for doc in db[coll].find():
    text = p.clean(doc['content'].encode('ascii', 'ignore').decode(encoding="utf-8"))
    doc['text'] = text
    hourStr = datetime.utcfromtimestamp(
        doc['time']/1000).strftime('%Y-%m-%d %H:00:00')
    time = datetime.utcfromtimestamp(
        doc['time']/1000).strftime('%Y-%m-%d %H:%M:%S')
    doc['hourStr'] = hourStr
    doc['timeStr'] = time
    db[coll].save(doc)


# build field arr
print('start to build field arr')
fieldArrs = {key: [] for key in fields}
docArr = []
for doc in db[coll].find():
    for idx, f in enumerate(fields):
        if fieldTypes[idx] == 'text':
            t = p.clean(doc[f].encode('ascii', 'ignore').decode(encoding="utf-8"))
            docArr.append(str(t))
        else:
            fieldArrs[f].append(str(doc[f]))

# build index arr
vectorizer = CountVectorizer()
fieldIdxArrs = {key: [] for key in fields}
X = None

print('start to build index arr')
fieldDicts = {key: {} for key in fields}
fieldSets = {key: [] for key in fields}
for idx, f in enumerate(fields):
    if fieldTypes[idx] == 'text':
        X = vectorizer.fit_transform(docArr)
        fieldIdxArrs[f] = X.tocoo().col.tolist()
        fieldSets[f] = vectorizer.get_feature_names()
    else:
        fieldSets[f] = list(sorted(set(fieldArrs[f]), key=fieldArrs[f].index))
        fieldDicts[f] = dict((val, idx)
                             for idx, val in enumerate(fieldSets[f]))

row = X.tocoo().row
data = X.tocoo().data

for r in row:
    for idx, f in enumerate(fields):
        if fieldTypes[idx] != 'text':
            fieldIdxArrs[f].append(fieldDicts[f][fieldArrs[f][r]])

print('start to decomp')
shape = tuple(len(fieldSets[key]) for key in fieldSets)
coords = tuple(fieldIdxArrs[key] for key in fieldIdxArrs)

tensorX = sptensor(coords, data, shape=shape, dtype=np.float)

# X_approx_ks,factors,lamb = nonnegative_tensor_factorization(tensorX, rank,max_iter=300)
# # factors = nonnegative_tensor_factorization(tensorX, rank)
# lamb = lamb.tolist()
# tensors = factors #list
# obj = {
#     "lmbda": lamb, #list
#     "dimArrs": [],
#     "id": coll,
# }

# print('start to write to db')
# for i, f in enumerate([key for key in fieldIdxArrs]):
#     dimObj = {}
#     coord = TSNE(n_components=2).fit_transform(tensors[i]).tolist()
#     arr = tensors[i].tolist()
#     temp = sorted(set(fieldIdxArrs[f]), key=fieldIdxArrs[f].index)
#     raw = [fieldSets[f][d] for d in temp]
#     maxRank = np.argmax(tensors[i], axis=1).tolist()
#     maxRankVal = np.amax(tensors[i], axis=1).tolist()
#     dimObj['dim'] = f
#     drawingArr = []
#     for idx, item in enumerate(coord):
#         drawingArr.append({
#             'coord': coord[idx],
#             'arr': arr[idx],
#             'raw': raw[idx],
#             'maxPattern': {'idx': maxRank[idx], 'value': maxRankVal[idx]},
#             'idx': idx
#         })
#     dimObj['drawingArr'] = drawingArr
#     dimObj['arr'] = arr
#     dimObj['patterns'] = np.transpose(arr).tolist()
#     dimObj['raw'] = raw
#     obj['dimArrs'].append(dimObj)

# db.tensorGov.drop()
# db.tensorGov.insert_one(obj)
# print('half-end')

error_ortho=[]
X_err=[]
corcond=[]
p=3 #rand

X_approx_ks,factors,lamb = nonnegative_tensor_factorization(tensorX, rank,max_iter=300)
error_orthogonal_sum=0

for i in range(4):
    s=factors[i].T.dot(factors[i])
    I=np.eye(s.shape[0])
    E=s-I
    error_orthogonal=np.linalg.norm(E)
    error_orthogonal_sum=error_orthogonal_sum+error_orthogonal

print(error_orthogonal_sum)
# X_approx = X_approx_ks.totensor()
    # X_error = (X - X_approx).norm() / X.norm()
        
# error_ortho.append(error_orthogonal_sum)
    # X_err.append(X_error)

    # plot: range  & orthogonal error
# x = list(range(p))
# y = error_ortho
# plt.figure(figsize=(8,4)) #创建绘图对象
# plt.plot(x,y,linewidth=1,color='coral')   #在当前绘图对象绘图（X轴，Y轴，蓝色p虚线，线宽度）
# plt.xlabel("Rank") #X轴标签
# plt.ylabel("Orthogonal error")  #Y轴标签
# plt.show()  #显示图}

print('end')