const db = require("./db");
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017";
const dbName = "mimic";
var moment = require("moment");

// drop original collections
var collectionName = "documents";

let clientTemp = new MongoClient(url);
clientTemp.connect(function(err) {
  const mdb = clientTemp.db(dbName);
  const collection = mdb.collection(collectionName);
  collection.drop();
  clientTemp.close();
});

db.getAllPatients().then(patients => {
  patients.forEach((p, idx) => {
    var ret = [];
    db.getPatientSeq(p.pid)
      .then(seqs => {
        for (let i = 0; i < seqs.length; i++) {
          seqs[i].timestamp = moment(seqs[i].event_time).unix();
        }

        seqs.sort((a, b) => a.timestamp - b.timestamp);

        var baseTime = seqs[0].timestamp;
        for (let i = 0; i < seqs.length; i++) {
          // seqs[i].relativeTime=seqs[i].timestamp-baseTime
          let sTemp = seqs[i];
          ret.push({
            gender: p.gender,
            pid: p.pid,
            icd_code: sTemp.icd_code,
            event_time: sTemp.event_time,
            event_type: sTemp.event_type,
            timestamp: sTemp.timestamp,
            relativeTime: ((seqs[i].timestamp - baseTime) / 86400) | 0
          });
        }
      })
      .then(() => {
        const client = new MongoClient(url);
        client.connect(function(err) {
          //   console.log("Connected successfully to server");

          const mdb = client.db(dbName);
          const collection = mdb.collection(collectionName);
          collection.insertMany(ret);

          client.close();
        });
      })
      .then(() => {
        if (idx % 500 == 0) {
          console.log(idx);
        }
      });
  });
});

// //  merge records with the same value in three dims
// db.documents
//   .aggregate([
//     {
//       $group: {
//         _id: {
//           pid: "$pid",
//           icd_code: "$icd_code",
//           relativeTime: "$relativeTime"
//         }, // can be grouped on multiple properties
//         dups: { $addToSet: "$_id" },
//         count: { $sum: 1 }
//       }
//     },
//     {
//       $match: {
//         count: { $gt: 1 } // Duplicates considered as count greater than one
//       }
//     }
//   ]) // You can display result until this and check duplicates
//   .forEach(function(doc) {
//     var first = doc.dups.shift(); // First element skipped for deleting
//     db.documents.update({ _id: first }, { $set: { count: doc.count } });
//     db.documents.remove({ _id: { $in: doc.dups } }); // Delete remaining duplicates
//   });
