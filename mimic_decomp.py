# import tensorly as tl
# import sparse
# from tensorly.decomposition import non_negative_parafac
from pymongo import MongoClient
import random
from ncp import *
from sktensor import sptensor
from scipy.sparse import *
import numpy as np
import json

# clientRemote = MongoClient('10.11.50.53', 27018)
clientLocal = MongoClient('localhost', 27017)
db = clientLocal.mimic
# arr = list(clientLocal.mimic.documents.find())
# clientRemote.mimic.documents.insert_many(arr)

patients = db.documents.find().distinct('pid')
patientsDict = dict((val, idx) for idx, val in enumerate(patients))

icds = db.documents.find().distinct('icd_code')
icdDict = dict((val, idx) for idx, val in enumerate(icds))

intervals = db.documents.find().distinct('relativeTime')
intervalDict = dict((val, idx) for idx, val in enumerate(intervals))

patientIdxArr = []
icdIdxArr = []
intervalIdxArr = []
data = []

for doc in db.documents.find():
    patientIdxArr += [patientsDict[doc[u'pid']]]
    icdIdxArr += [icdDict[doc[u'icd_code']]]
    intervalIdxArr += [intervalDict[doc[u'relativeTime']]]
    data += [doc[u'count']]

shape = (len(patients), len(icds), len(intervals))

coords = (patientIdxArr, icdIdxArr, intervalIdxArr)
X = sptensor(coords, data, shape=shape, dtype=np.float)

factors = nonnegative_tensor_factorization(X, 3)
print('shape: ', factors.shape)
print('ndim: ', factors.ndim)
print('rank: ', factors.rank)
print('lambda: ', factors.lmbda)
print('U; ', len(factors.U))
print('U; ', type(factors.U[0]))
print('U; ', factors.U[0])


class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


print(type(factors.U[0]))
obj = {
    "shape": factors.shape,
    "ndim": factors.ndim,
    "rank": factors.rank,
    "lmbda": factors.lmbda.tolist(),
    "tensors": [d.tolist() for d in factors.U]
}

db.mimic_tensor.drop()
db.mimic_tensor.insert_one(obj)
print('end')
